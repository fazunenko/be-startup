/**
 * This is just helper scrip to deal with DB. It is not used by the application.
 * It helps to create users used by tests.
 */

const MongoClient = require('mongodb').MongoClient;
const settings = require('../settings')

go();

async function go() {
    const client = await MongoClient.connect(settings.dbUri)
    const DB = client.db('experimentalDB')
    try {
        await addUser(TEST_USER, DB)
        await addUser(TEST_ADMIN, DB)
        await printUsers(DB)
    } catch (e) {
        console.log(e)
    }
    await client.close()
}

async function printUsers(db) {
    const DB_USERS = db.collection('users')
    DB_USERS.find({}).toArray((err,result) => {
        if (err) throw err
        for (let u of result) {
            console.log(u)
        }
    })
}

const TEST_ADMIN = {
    user: 't_admin',
    password: '81__23hya8efd5',
    email: 't_admin@example.com',
    fullName: 'Mr. Amdin',
    access: 255
}

const TEST_USER = {
    user: 't_user',
    password: 'e__22a36f7900161',
    email: 't_user@example.com',
    fullName: 'Mr. User',
    access: 0
}

async function addUser(data, db) {
    const DB_USERS = db.collection('users')
    const DB_PROFILES = db.collection('personal')

    const passLib = require('../routes/pass.lib')
    let password = data.password
    let salt = passLib.genRandomString(17)
    let newUser = {
        user: data.user,
        salt: salt,
        hmac: passLib.hmac(password, salt),
        access: data.access
    }
    let newProfile = {
        user: data.user,
        email: data.email,
        fullName: data.fullName
    }

    let user = DB_USERS.findOne({user: data.user})
    if (user) {
        console.log('Failed to add user: ')
        console.log(data)
        console.log('Already exists')
        return
    }
    await DB_USERS.insertOne(newUser)
    await DB_PROFILES.insertOne(newProfile)
    console.log(data.user + ": successfully added")
}

