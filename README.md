# BackEnd start up

This project is a Node JS back end server based on 
**express** and **MongoDB**. It implements REST API for 
some basic functionality and could be used as a starting 
point for real servers.
 
## This project demonstrates how to:
* work with MongoDB
* store passwords in DB
* verify authentication
* define endpoints (REST API)
* endpoints with different access level: 
  * authorization is not required
  * authorization is required
  * authorization is required and user has admin rights
* develop tests

## Implemented API

* `POST /login {user,password}` returns `{token}`
* `GET /version` returns `{version, date}`
* `GET /home/profile` returns `{profile: {email,fullName}}`
* `PUT /home/profile {email, fullName}` updates own profile
* `PUT /home/password {oldp, newp}` updates own password
* `GET /admin/users` returns `{users: []}`
* `POST /admin/user {user, password, email, fullName}` adds new user 
* `PUT /admin/user {user, email, fullName}` updates user's profile
* `PATCH /admin/user {user}` resets user's password
* `GET /admin/user/:user` returns `{profile: {user,email,fullName,access}}`
* `DELETE /admin/user/:user` deletes user

## Starting the server locally

* Clone the repository and init Node JS libraries:
```
#> git clone https://bitbucket.org/fazunenko/be-startup.git
#> cd be-startup
#> npm i
#> npm audit fix --force  
``` 
* Put your _local port_ and _DB URI_ into **./settings.js** 
* Start your app
```
#> npm start 
```
* Open http://localhost:3001/read/version to see if your local server works.

## How to config MongoDB free cluster
* Go to https://www.mongodb.com/free-cloud-database
* Sign up
* Create a project
* Create a cluster
* Create a new user with read/write access(**Database access**)
* By default, the access to DB is limited by a single IP Address. 
  You probably will need to add more IP Addresses or allow connection from anywhere.
  This could be done from **Network Access** 
* To find URI to connect from your server: 
  * Go to Clusters -> [CONNECT] -> Connect your application
  * Find line like: _const uri = "mongodb+srv://\<username>:\<password>@cluster0.z610b.mongodb.net/<dbname>?retryWrites=true&w=majority";_
  * Replace _\<username>_ and _\<password>_ with those which you used for creating the user.
  * You can use any name for _\<dbname>_

## Starting the tests
```
#> npm test 
```


