'use strict'
require('./test_global.js')

let WebClient = function() {
    let self = this; // to be used inside methods

    const baseURI = 'http://localhost:' + require('../settings').port
    let token
    let superagent = require('superagent')

    function filterError(err) {
        if (err.response) {
            return {
                status: err.response.status,
                error: JSON.parse(err.response.text).error
            }
        } else {
            throw err
        }
    }

    function request(method, endpoint, data) {
        let p = method(baseURI + endpoint)
        if (data) {
            p = p.type('json').send(JSON.stringify(data))
        }
        if (token) {
            p.set('x-access-token', token)
        }
        return p.catch(filterError)
    }

    this.get = function (endpoint) {
        note('GET ' + endpoint)
        return request(superagent.get, endpoint)
    }

    this.post = function (endpoint, data) {
        note('POST ' + endpoint)
        return request(superagent.post, endpoint, data)
    }
    this.put = function (endpoint, data) {
        note('PUT ' + endpoint)
        return request(superagent.put, endpoint, data)
    }
    this.patch = function (endpoint, data) {
        note('PATCH ' + endpoint)
        return request(superagent.patch, endpoint, data)
    }
    this.delete = function (endpoint, data) {
        note('DELETE ' + endpoint)
        return request(superagent.delete, endpoint, data)
    }

    this.ping = function () {
        note('PING ')
        return request(superagent.get, EP.VERSION).then(res => true).catch(e => false)
    }

    this.updateToken = function(newToken) {
        token = newToken
    }

    this.login = async function (data) {
        let res = await this.post(EP.LOGIN,data)
        token = res.status === 200 ? JSON.parse(res.text).token : undefined
        if (!token) {
            throw Error('Login failed')
        }
    }

    this.logout = function () {
        token = undefined
    }
}
module.exports = new WebClient()

