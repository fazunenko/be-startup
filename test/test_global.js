require('../global')

global.web = require('./web.lib')
global.chai = require('chai');
global.expect = chai.expect;

global.js = function(obj) {
    return obj === null ? null : JSON.stringify(obj,undefined,4)
}

global.expect200 = function (res) {
    chai.expect(res.status).to.eq(200, js(res))
    return res
}

global.expectError = function (res, status, message) {
    chai.expect(res.status).to.eq(status, 'status')
    if (message !== undefined) {
        let errMessage = res.error
        if (message instanceof RegExp) {
            chai.expect(errMessage).to.match(message, 'error message')
        } else {
            chai.expect(errMessage).to.eq(message, 'error message')
        }
    }
    return res
}

global.matchJson = function (actual, expected, ignore = []) {
    let isPassed = _subEqRec(actual, expected, null, ignore)
    if (!isPassed) {
        notejs(actual, 'Actual: ')
        notejs(expected, 'Expected: ')
    }
    return isPassed
}

global.expectMatchJson = function (actual, expected, ignore = []) {
    if (!matchJson(actual, expected, ignore)) {
        throw Error('match json failed')
    }
}

function _subEqRec(actual, expected, key, ignore) {
    if (actual == expected || ignore.includes(key)) {
        return true
    }
    if (actual == null) {
        console.log(`value of ${key} differ`)
        console.log(`  actual  : ${actual}`)
        console.log(`  expected: ${expected}`)
        return false
    }
    if (typeof expected !== 'object') {
        if (actual === expected) {
            return true
        } else {
            console.log(`value of ${key} differ`)
            console.log(`  actual  : ${actual}`)
            console.log(`  expected: ${expected}`)
            return false
        }
    } else {
        if (expected instanceof RegExp) {
            if (actual === undefined) {
                console.log(`value of ${key} is undefined`)
                console.log(`  expected to match: ${expected}`)
                return false
            }
            actual = '' + actual // convert to string
            if (actual.match(expected)) {
                return true
            } else {
                console.log(`value of ${key} doesn't match ${expected}`)
                console.log(`  actual  : ${actual}`)
                return false
            }
        } else if (typeof actual === 'object') {
            let pass = true
            for (let f of Object.keys(expected)) {
                let exp = expected[f] === null ? undefined : expected[f]
                let act = actual[f]
                let newKey = key ? key + '.' + f : f
                let nextPass = ignore.includes(f) || _subEqRec(act, exp, newKey, ignore)
                pass =  pass && nextPass
            }
            return pass
        } else {
            console.log(`value of ${key} is not an object`)
            console.log(`  actual  : ${actual}`)
            console.log(`expected  : ${expected}`)
            return false
        }
    }
}
