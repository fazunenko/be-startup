require('./test_global')


const THE_ADMIN = {
    user: 't_admin',
    password: '81__23hya8efd5'
}
const THE_ADMIN_PROFILE = {
    email: 't_admin@example.com',
    fullName: 'Mr. Amdin'
}

const THE_USER = {
    user: 't_user',
    password: 'e__22a36f7900161'
}
const THE_USER_PROFILE = {
    email: 't_user@example.com',
    fullName: 'Mr. User'
}

const DEFAULT_PASSWORD = 'pepsi'

function loginAdmin() {
    return web.login(THE_ADMIN)
}
function loginUser() {
    return web.login(THE_USER)
}

let shutdownKey
async function startBE() {
    this.timeout(5000)
    let isServerUp = await web.ping()
    if (!isServerUp) {
        shutdownKey = require('../bin/www').shutdown
        notejs(shutdownKey, '^^^^')
        for (let i = 0; i < 20  && !isServerUp; i++) {
            await sleep(1000)
            isServerUp = await web.ping()
            notejs(isServerUp, `#${i}: `)
        }
    }
}
function stopBE() {
    if (shutdownKey) {
        return web.get('/' + shutdownKey)
    }
}

before(startBE)
after(stopBE)


describe('login', function () {
    beforeEach(web.logout)

    it('passed admin', async function () {
        await web.login(THE_ADMIN)
    })

    it('passed user', async function () {
        await web.login(THE_USER)
    })

    let badPasswordCases = [
        {user: THE_ADMIN.user, pass: 'xxx', title: 'bad password', status: 401, msg: 'User/Password mismatch'},
        {user: THE_ADMIN.user, pass: '',  title: 'empty password', status: 401, msg: 'User/Password mismatch'},
        {user: 'asfasdfsda',   pass: THE_ADMIN.password,  title: 'unknown user', status: 401, msg: 'User/Password mismatch'},
        {user: '', pass: THE_ADMIN.password,  title: 'empty user', status: 400, msg: 'No user'},
        {user: '', pass: '',  title: 'empty user and password', status: 400, msg: 'No user'},
    ]

    for (let tc of badPasswordCases) {
        it(`reject ${tc.title}`, async function () {
            expectError( await web.post(EP.LOGIN, {user: tc.user, password: tc.pass}), tc.status, tc.msg )
        })
    }
})

describe('version', function () {
    it('check verion', async function () {
        await web.logout()
        let ver = expect200(await web.get(EP.VERSION)).body
        expect(ver.version).to.eq('0.1')
        expect(ver.date).to.satisfy(x => x.startsWith("202"))
    })

})

describe('password', function () {
    before(loginUser)

    it('password: incorrect old', async function () {
        expectError(await web.put(EP.PASSWORD, {oldp: 'bad', newp: 'TwoClubs'}), 403, 'Old password mismatch' )
    })

    it('password: incorrect new', async function () {
        expectError(await web.put(EP.PASSWORD, {oldp: 'OneClub', newp: ''}), 400, 'Bad data' )
    })
})

describe('profile', function () {
    this.timeout(5000)
    before(web.logout)

    it('Unauthorized GET', async function () {
        expectError(await web.get(EP.PROFILE), 401, 'Not authorized' )
    })

    it('Unauthorized PUT', async function () {
        expectError(await web.put(EP.PROFILE, {email: 'yes@example.com', fullName: 'Yes No'}), 401, 'Not authorized' )
    })

    it('get user profile', async  function () {
        await web.login(THE_USER)
        let res = expect200(await web.get(EP.PROFILE))
        notejs(res.body, 'THE USER profile')
        let profile = res.body.profile
        expect(profile.user).to.eq(THE_USER.user)
        expect(profile.fullName).to.eq(THE_USER_PROFILE.fullName)
        expect(profile.email).to.eq(THE_USER_PROFILE.email)
        expect(profile.isAdmin).to.eq(false)

    })

    it('get admin profile', async function () {
        await web.login(THE_ADMIN)
        let res = expect200(await web.get(EP.PROFILE))
        notejs(res.body, 'THE ADMIN profile')
        let profile = res.body.profile
        expect(profile.user).to.eq(THE_ADMIN.user)
        expect(profile.fullName).to.eq(THE_ADMIN_PROFILE.fullName)
        expect(profile.email).to.eq(THE_ADMIN_PROFILE.email)
        expect(profile.isAdmin).to.eq(true)
    })

})

describe('admin', function () {
    const NEW_USER = {user: 'abc', password: 'abc', fullName: 'Mr ABC', email: 'mr.ABC@example.com'}
    const fields = ['user', 'fullName', 'email']

    before(loginAdmin)

    for (let f of fields) {
        it('add new, bad ' + f, async function () {
            let invalid = Object.assign({}, NEW_USER)
            invalid[f] = undefined
            expectError(await web.post(EP.USER, invalid), 400, `incorrect ${f}`)
        })
    }

    it('add new', async function () {
        expect200( await web.post(EP.USER, NEW_USER) )
    })

    it('add duplicate', async function () {
        expectError(await web.post(EP.USER, NEW_USER), 412, 'Already exists')
    })

    it('get', async function () {
        let res = expect200(await web.get(EP.USER + '/' + NEW_USER.user) )
        let profile = res.body.profile
        expect(profile.user).to.eq(NEW_USER.user)
        expect(profile.fullName).to.eq(NEW_USER.fullName)
        expect(profile.email).to.eq(NEW_USER.email)
    })

    it('delete', async function () {
        await loginAdmin()
        expect200( await web.delete(EP.USER + '/' + NEW_USER.user) )
    })

    it('user access', async function () {
        await loginUser()
        expectError(await web.post(EP.USER, NEW_USER), 403, 'Not allowed' )
    })

})

describe('new created user', function () {
    this.timeout(5000)
    let password = 'xyz'
    const NEW_USER = {user: 'abc', password, fullName: 'Mr ABC', email: 'mr.ABC@example.com'}
    const UPDATE_PROFILE = {fullName: 'Mrs XYZ', email: 'mrs.xyz@example.com'}
    const UPDATE_PASSWORD = 'Pass' + new Date().getTime()

    before(async function () {
        await loginAdmin()
        expect200( await web.post(EP.USER, NEW_USER))
    })

    after(async function () {
        await loginAdmin()
        expect200( await web.delete(EP.USER + '/' + NEW_USER.user))
    })

    it('check get profile', async function () {
        await web.login({user: NEW_USER.user, password})
        let res = expect200(await web.get(EP.PROFILE))
        notejs(res.body, 'profile')
        let profile = res.body.profile
        expect(profile.user).to.eq(NEW_USER.user)
        expect(profile.fullName).to.eq(NEW_USER.fullName)
        expect(profile.email).to.eq(NEW_USER.email)
        expect(profile.isAdmin).to.eq(false)
    })

    it('check update profile', async function () {
        await web.login({user: NEW_USER.user, password})
        let res = expect200(await  web.put(EP.PROFILE, UPDATE_PROFILE))
        expect200(res)
        notejs(res.body, 'update')
        let profile = res.body.profile
        expect(profile.user).to.eq(NEW_USER.user)
        expect(profile.fullName).to.eq(UPDATE_PROFILE.fullName)
        expect(profile.email).to.eq(UPDATE_PROFILE.email)
        expect(profile.isAdmin).to.eq(false)
    })

    it ('check update password', async function () {
        await web.login({user: NEW_USER.user, password})
        let res = expect200(await web.put(EP.PASSWORD, {oldp: password, newp: UPDATE_PASSWORD}) )
        expect(res.body).to.not.eq(undefined, 'body')
        let newToken = res.body.token
        expect(newToken).to.not.eq(undefined, 'new token')
        web.updateToken(newToken)

        res = expect200(await web.get(EP.PROFILE))
        let profile = res.body.profile
        expect(profile.user).to.eq(NEW_USER.user)
        expect(profile.fullName).to.eq(UPDATE_PROFILE.fullName)
        expect(profile.email).to.eq(UPDATE_PROFILE.email)
        expect(profile.isAdmin).to.eq(false)
    })

    it ('check login with new password password', async function () {
        await web.login({user: NEW_USER.user, password: UPDATE_PASSWORD})
    })

    it ('check reset to default', async function () {
        await loginAdmin()
        expect200(await web.patch(EP.USER, {user: NEW_USER.user}))
        expectError(await web.post(EP.LOGIN, {user: NEW_USER.user, password: UPDATE_PASSWORD}), 401, 'User/Password mismatch' )
    })

    it ('check login with default password', async function () {
        await web.login({user: NEW_USER.user, password: DEFAULT_PASSWORD})
    })

})

describe('users', function () {
    before(loginAdmin)
    it('get', async function () {
        let res = expect200(await web.get(EP.USERS))
        let users = res.body.users
        expect(users.length).to.be.above(0, 'length')
    })
})



