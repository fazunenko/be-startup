require('./global')

const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const settings = require('./settings')
const MongoClient = require('mongodb').MongoClient

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const endPoints = require('./routes')
endPoints.configPermissions(app, express)

MongoClient.connect(settings.dbUri).then(async (client) => {
  const DB = client.db('experimentalDB')
  endPoints.configDB(app, DB)
}).catch(err => console.log(err))

module.exports = app;
