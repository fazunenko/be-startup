require('../global')

const JWT = require('jsonwebtoken')
const settings = require('../settings')
const JWT_SECRETE = settings.secrete

function decodeToken(token) {
    return JWT.verify(token, JWT_SECRETE, (err, decoded) => {
        if (err) {
            note('bad token: ' + JSON.stringify(token))
            return undefined
        } else {
            // if everything is good, save to request for use in other routes
            let now = Date.now() / 1000 | 0
            if (decoded.exp < now) {
                note('expiered token')
                return undefined
            }
            return decoded
        }
    })
}

function findToken(req) {
    return req.body.token || req.query.token || req.headers['x-access-token']
}

function needValidToken(req, res, next, isAdmin) {
    printRequest(req)
    let token = findToken(req)
    if (token) {
        let decoded = decodeToken(token)
        if (decoded && (!isAdmin || decoded.isAdmin)) {
            req.decoded = decoded
            printUser(decoded)
            next()
        } else {
            res.status(403).json({error: 'Not allowed'})
        }
    } else {
        res.status(401).json({error: 'Not authorized'})
    }
}

function optionalToken(req, res, next) {
    printRequest(req)
    let decoded
    let token = findToken(req)
    if (token) {
        decoded = decodeToken(token)
    }
    req.decoded = decoded ? decoded : {}
    printUser(decoded)
    next()
}


function printRequest(req) {
    console.log('---+ ' + req.method + ' +--- ' + req.baseUrl)
}
function printUser(decoded) {
    if (decoded && decoded.user) {
        console.log('  by: ' + decoded.user)
    } else {
        console.log('  by none')
    }

}


module.exports = {
    requireAdmin: function (req, res, next) {
        needValidToken(req, res, next, true)
    },

    requireLogin: function (req, res, next) {
        needValidToken(req, res, next, false)
    },

    optionalLogin: function (req, res, next) {
        optionalToken(req, res, next)
    }
}