require('../global')

module.exports = function (app, db) {
    app.route(EP.VERSION)
        .get(handleErrorAsync(async (req, res) => {
            let version = VERSION
            return res.json(version)
        }))
}
