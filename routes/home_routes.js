require('../global')
const passLib = require('./pass.lib')
const validator = require('./validator')

async function getProfile(db, user, isAdmin) {
    let profile = await db.findOne({user: user})
    if (profile) {
        profile._id = undefined
        profile.isAdmin = isAdmin
    }
    return profile
}

module.exports = function (app, db) {
    const DB_PERSONAL = db.collection('personal')
    const DB_USERS = db.collection('users')

    app.route(EP.PROFILE)
        .get(handleErrorAsync(async (req, res) => {
            let profile = await getProfile(DB_PERSONAL, req.decoded.user, req.decoded.isAdmin)
            return res.json({profile})
        }))
        .put(handleErrorAsync(async (req, res) => {
            if (!req.body || !req.body.email || !req.body.fullName) {
                return res.status(400).json({error: 'No data'})
            }
            let update = {
                email: req.body.email.trim(),
                fullName: req.body.fullName.trim()
            }
            if (!validator.checkEmail(update.email) || !validator.checkFullName(update.fullName)) {
                return res.status(400).json({error: 'Incorrect data'})
            }

            const user = req.decoded.user
            const isAdmin = req.decoded.isAdmin

            await DB_PERSONAL.updateOne({user: user}, {$set: update})
            let profile = await getProfile(DB_PERSONAL, user, isAdmin)
            return res.json({profile})
        }))

    app.route(EP.PASSWORD)
        .put(handleErrorAsync(async (req, res) => {
            if (!req.body || !req.body.oldp || !validator.checkPassword(req.body.newp)) {
                return res.status(400).json({error: 'Bad data'})
            }
            const user = req.decoded.user
            const oldp = req.body.oldp
            const newp = req.body.newp

            let item = await DB_USERS.findOne({user: user})
            if (!item) {
                return res.status(401).json({error: 'User/Password mismatch'})
            }

            let salt = item.salt
            let hmac = passLib.hmac(oldp, salt)
            if (hmac !== item.hmac) {
                return res.status(403).json({error: 'Old password mismatch'})
            }
            salt = passLib.genRandomString(17)
            let update = {
                salt: salt,
                hmac: passLib.hmac(newp, salt)
            }
            await DB_USERS.updateOne({user: user}, {$set: update})
            return res.json({token: passLib.genToken(user, item.access)})
        }))

}
