require('../global')
const passLib = require('./pass.lib')

module.exports = function (app, db) {
    const DB_USERS = db.collection('users')
    app.route(EP.LOGIN)
      .post(handleErrorAsync( async (req, res) => {
        notejs(req.body, '--> login: ')
        if (!req.body) {
            notejs('  --> login: No body')
            return res.status(400).json({error:'No body'})
        }

        let body = typeof req.body === 'string' ? JSON.parse(req.body) : req.body
        let user = body.user
        let password = body.password

        if (!user) {
            notejs('  --> login: No user')
            return res.status(400).json({error: 'No user'})
        }

        let item = await DB_USERS.findOne({user: user})
        if (!item) {
            notejs('  --> login: User/Password mismatch (1)')
            return res.status(401).json({error: 'User/Password mismatch'})
        }
        let salt = item.salt
        let hmac = passLib.hmac(password, salt)
        if (hmac === item.hmac) {
            notejs('  --> login: success')
            return res.json({token: passLib.genToken(user, item.access)})
        } else {
            notejs('  --> login: User/Password mismatch (2)')
            return res.status(401).json({error: 'User/Password mismatch'})
        }
    }))

}
