const guard = require('./guard')


function configDB (app, db) {
    require('./home_routes')(app, db)
    require('./login_routes')(app, db)
    require('./admin_routes')(app, db)
    require('./read_routes')(app, db)
}

function configPermissions(app, express) {
    let apiRoutesHome = express.Router()
    apiRoutesHome.use(guard.requireLogin)
    app.use('/home/*', apiRoutesHome)

    let apiRoutesAdmin = express.Router()
    apiRoutesAdmin.use(guard.requireAdmin)
    app.use('/admin/*', apiRoutesAdmin)

    let apiRoutesRead = express.Router()
    apiRoutesRead.use(guard.optionalLogin)
    app.use('/read/*', apiRoutesRead)
}


module.exports = {configDB, configPermissions}
