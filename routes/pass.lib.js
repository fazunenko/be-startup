'use strict'
const crypto = require('crypto')
const JWT = require('jsonwebtoken')
const JWT_SECRETE = require('../settings').secrete


function passLib() {
    this.genRandomString = function (length) {
        return crypto.randomBytes(Math.ceil(length / 2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0, length)
    }

    this.hmac = function (password, salt) {
        let hash = crypto.createHmac('sha1', salt)
        hash.update(password)
        return hash.digest('hex')
    }

    this.genToken = function (user, access) {
        let isAdmin = access ===  255
        return JWT.sign({user: user, isAdmin: isAdmin}, JWT_SECRETE, {expiresIn: 144000})
    }

}


function saltHashPassword (userpassword) {
    let lib = new passLib()
    let salt = lib.genRandomString(17) // Gives us salt of length 16
    let hmac = lib.hmac(userpassword, salt)
    console.log('UserPassword = ' + userpassword)
    console.log('hmac = ' + hmac)
    console.log('salt = ' + salt)
}
saltHashPassword('8a87bd0c5f6c8efd5')


module.exports = new passLib()
