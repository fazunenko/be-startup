require('../global')
const passLib = require('./pass.lib')
const DEFAULT_PASSWORD = 'pepsi'
const validator = require('./validator')

function trim(data) {
    if (!data) {
        return
    }
    Object.keys(data).forEach(function (f) {
        if (typeof data[f] === 'string') {
            data[f] = data[f].trim()
        }
    })
    return data
}

function validate(data, what) {
    if (!data) {
        return 'no data'
    }

    if (what.includes('user') && (!data.user || !validator.checkLogin(data.user))) {
        return 'incorrect user'
    }
    if (what.includes('password') && (!data.password || !validator.checkPassword(data.password))) {
        return 'incorrect password'
    }
    if (what.includes('email') && (!data.email || !validator.checkEmail(data.email))) {
        return 'incorrect email'
    }
    if (what.includes('fullName') && (!data.fullName || !validator.checkFullName(data.fullName))) {
        return 'incorrect fullName'
    }
    return undefined
}

function checkNotFound(item) {
    if (item) {
        throw HandleError({message: 'Already exists', status: 412})
    }
}

module.exports = function (app, db) {
    const DB_USERS = db.collection('users')
    const DB_PROFILES = db.collection('personal')

    app.route(EP.USERS)
        .get(handleErrorAsync(async (req, res) => {
            let users = await DB_PROFILES.find({}, {fields: {_id: 0, user: 1, fullName: 1, email: 1}}).toArray()
            res.json({users})
        }))
    app.route(EP.USER)
        .post(handleErrorAsync(async (req, res) => {
            let data = trim(Object.assign({}, req.body))
            let err = validate(data, ['user', 'email', 'fullName'])
            if (err) {
                note('Invalid request: ' + err)
                return res.status(400).json({error: err})
            }
            let password = data.password || DEFAULT_PASSWORD
            let salt = passLib.genRandomString(17)
            let newUser = {
                user: data.user,
                salt: salt,
                hmac: passLib.hmac(password, salt),
                access: 0
            }
            let newProfile = {
                user: data.user,
                email: data.email,
                fullName: data.fullName
            }

            let item = await DB_USERS.findOne({user: data.user})
            checkNotFound(item)
            item = await DB_PROFILES.findOne({user: data.user})
            checkNotFound(item)
            await DB_USERS.insertOne(newUser)
            await DB_PROFILES.insertOne(newProfile)
            return res.json({})
        }))
        .put(handleErrorAsync(async (req, res) => {
            let data = trim(Object.assign({}, req.body))
            let err = validate(data, ['email', 'fullName'])
            if (err) {
                note('Invalid request: ' + err)
                return res.status(400).json({error: 'Invalid request'})
            }
            let update = {
                email: data.email,
                fullName: data.fullName
            }
            DB_PROFILES.updateOne({user: data.user}, {$set: update})
                .then(() => res.json({}))
                .catch(err => {
                    note('Failed to update user: ' + err)
                    res.status(412).json({error: 'Failed to update user'})
                })
        } ))
        .patch(handleErrorAsync(async (req, res) => {
            const user = req.body.user
            if (!user) {
                note('Invalid request')
                return res.status(400).json({error: 'Invalid request'})
            }

            note('Patch user: ' + user)
            let salt = passLib.genRandomString(17)
            let passwordReset = {
                salt: salt,
                hmac: passLib.hmac(DEFAULT_PASSWORD, salt),
            }
            DB_USERS.updateOne({user: user}, {$set: passwordReset})
                .then(() => res.json({}))
                .catch(err => {
                    note('Failed to reset user password: ' + err)
                    return res.status(412).json({error: 'Failed to reset user password'})
                })
        } ))

    app.route(EP.USER_INFO)
        .get(handleErrorAsync(async (req, res) => {
            let user = req.params.user
            note('Get user profile ' + user)

            DB_PROFILES.findOne({user: user})
                .then((profile) => {

                    let ans = {profile: {
                        user: user,
                        email: profile.email,
                        fullName: profile.fullName,
                        access: profile.access
                    }}
                    res.json(ans)

                })
                .catch(err => {
                    note('Failed to remove new user: ' + err)
                    res.status(412).json({error: 'Failed to remove user'})
                })
        } ))
        .delete(handleErrorAsync(async (req, res) => {
            let user = req.params.user
            note('Delete user: ' + user)

            DB_USERS.removeOne({user: user})
                .then(() => DB_PROFILES.removeOne({user: user}))
                .then(() => res.json({}))
                .catch(err => {
                    note('Failed to remove new user: ' + err)
                    res.status(412).json({error: 'Failed to remove user'})
                })
        } ))

}
