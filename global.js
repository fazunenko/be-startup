global.js = function (json) {
    return JSON.stringify(json, undefined, 4)
}

global.notejs = function (json, msg = '') {
    console.log(msg + js(json))
}

global.note = function (msg) {
    console.log(msg)
}

global.cloneObj = function(obj) {
    return JSON.parse(JSON.stringify(obj))
}

global.sleep = function (millis) {
    return new Promise(f => setTimeout(f, millis))
}

global.VERSION = {version: '0.1', date: new Date()}

global.EP = {
    format: function (ep, params){
        for (let k of Object.keys(params)) {
            ep = ep.replace(`:${k}/`, params[k] + '/').replace(new RegExp(`:${k}$`), params[k])
        }
        return ep
    },

    LOGIN:      '/login',
    VERSION:    '/read/version',
    PROFILE:    '/home/profile',
    PASSWORD:   '/home/password',

    USER:       '/admin/user',
    USER_INFO:  '/admin/user/:user',
    user_info:  user => EP.format(EP.USER_INFO, {user}),
    USERS:      '/admin/users'
}

global.handleErrorAsync = function(func, comment = '') {
    return async (req, res, next) => {
        try {
            await func(req, res, next);
        } catch (error) {
            notejs(error.message, 'Error message: ' + comment)
            let status = error.status ? error.status : 500
            res.status(status).json({error: error.message})
        }
    }
}


global.HandleError = function({message, status = 500}) {
    return {
        message,
        status,
        stack: new Error().stack
    }
}



global.ObjectId = require('mongodb').ObjectID